# Scenario Scadenza Manutenzione

## Requirements Class

Rental Needs

## Description

Questo scenario positivo illustra un evento in cui un utente noleggia uno scooter, inizia a guidare e si verifica un guasto sullo scooter.

## Structure

### Context: 

L'azienda desidera realizzare un sistema software per la gestione dei noleggi e la manutenzione degli scooter. Gli utenti possono noleggiare gli scooter dopo aver fornito il proprio documento d'identità. Il sistema tiene traccia del percorso e delle condizioni degli scooter durante il noleggio.

### Scenario

Un utente, utilizzando l'applicazione di noleggio scooter, decide di noleggiarne uno per spostarsi in città. Dopo aver fornito il proprio documento d'identità per la registrazione, inizia il noleggio e inizia a guidare lo scooter.
Durante il noleggio, lo scooter presenta un guasto improvviso, rallentando e attivando la spia di "manutenzione". L'utente si ferma; nel frattempo il sistema di gestione degli scooter rileva immediatamente il problema e invia una notifica al manutentore della zona.

## Stakeholders

Alessandro Gilardi

## Reason

Questo scenario è stato creato per dimostrare come il sistema di gestione degli scooter affronti situazioni di guasto meccanico durante un noleggio. Il sistema è in grado di rilevare il problema e notificare il manutentore, come previsto nei requisiti.
