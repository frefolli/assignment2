# Scenario Batteria Scarica

## Requirements Class

Requisiti in merito al dover gestire scenari di "batteria scarica" dei mezzi.
Requisiti in merito al dover comunicare con un sistema terzo per la gestione degli interventi.

## Description

Questa attività è uno Scenario Anormale. 
In questo Scenario ipotizziamo che la batteria di un mezzo diventi insufficientemente carica.
Nota: va definita a priori una soglia numerica che permetta di ritenere i mezzi con carica inferiore scarichi.
Il sistema, non appena il cliente finisce il noleggio, cambia lo stato del mezzo in non noleggiabile e dispone la richiesta di un intervento di un addetto che vada a ricaricare il mezzo.


## Stakeholders

Lo scenario è mostrato ad Alessandro Gilardi di persona, si raccolgono le sue impressioni direttamente come spunto per i lavori successivi.

## Reason

Consideriamo questo scenario perchè capiterà spesso che le batterie dei mezzi si scarichino ed è fondamentale la comprensione del da farsi nel gestirlo.
Contattiamo il cliente per avere riscontro sulla nostra effettiva comprensione di questo requisito del sistema, sottoponendogli questo scenario.
