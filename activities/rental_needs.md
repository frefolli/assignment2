# Rental Needs

## Requirements Class

Questa attività estrae i requisiti relativi all'attività di tracciamento dei dati di noleggio.

## Description

Un'intervista ad alcuni stakeholder sulle loro esigenze legate all'attività di noleggio e, in particolare, al trattamento dei dati degli utenti, degli scooter e degli eventuali guasti o interventi di manutenzione necessari. Sono stati prodotti come artefatto i dati grezzi dell'intervista, un modello di dominio aggiornato per tale sottosistema e l'elenco dei requisiti emersi.

## Stakeholders

Alegil

## Reason

Chiediamo direttamente ai clienti quali sono le loro reali esigenze per affinare la nostra visione sul sistema che sarà.

## Structure

1. Registrare i dati GPS (e altro):
    1. È importante tenere traccia della posizione esatta dei monopattini durante il noleggio? Se sì, in che modo pensi di utilizzare queste informazioni?
    2. Vuoi registrare la velocità e il percorso seguito dai monopattini durante il noleggio per scopi analitici o di sicurezza?
    3. Dovrebbero essere registrate anche informazioni sulle condizioni stradali o meteorologiche durante il noleggio?

2. Registrare le informazioni personali dell'utente:
    1. Hai esigenze particolari per la registrazione di informazioni relative all'utente che noleggia il monopattino?
    2. Pensi che la registrazione di foto dell'utente sia necessaria per scopi di sicurezza o verifica?
    3. Come affronteresti la gestione dei dati personali in conformità alle normative sulla privacy?

3. Analizzare il comportamento del conducente:
    1. Pensi di voler analizzare il comportamento dei conducenti durante il noleggio? In caso affermativo, quali metriche o comportamenti specifici desideri monitorare?
    2. Come useresti i dati sul comportamento dei conducenti per migliorare la sicurezza e l'efficienza del servizio?
    3. Hai intenzione di fornire feedback agli utenti basati sul loro comportamento durante il noleggio?

4. Segnalare automaticamente gli scooter che necessitano di manutenzione:
    1. Vorresti che il sistema identificasse automaticamente i monopattini che richiedono manutenzione in base ai dati raccolti durante il noleggio? Se sì, quali indicatori o segnali dovrebbe considerare?
    2. Quali tipi di problemi meccanici o di sicurezza ritieni dovrebbero innescare automaticamente una segnalazione di manutenzione?
    3. Hai preferenze sulla modalità di notifica quando un monopattino richiede manutenzione?

5. Conclusione:
    1. C'e' qualche altra feature di cui non abbiamo parlato che vorresti analizzare?
    2. C'e' qualche osservazione che a posteriori ti senti di dare sull'intervista?

## Raw Data Collection

Si riportano per intero le risposte dell'intervistato.

1. Registrare i dati GPS (e altro): 
    1. È importante tenere traccia della posizione esatta dei monopattini durante il noleggio? Se sì, in che modo pensi di utilizzare queste informazioni? Sì, per una questione di sicurezza voglio tener traccia di dove sono in ogni momento e di dove sono stati nel passato, oltre allo scopo analitico
    2. Vuoi registrare la velocità e il percorso seguito dai monopattini durante il noleggio per scopi analitici o di sicurezza? Entrambi
    3. Dovrebbero essere registrate anche informazioni sulle condizioni stradali o meteorologiche durante il noleggio? Per ora non è necessario, magari in futuro
 
2. Registrare le informazioni personali dell'utente: 
    1. Hai esigenze particolari per la registrazione di informazioni relative all'utente che noleggia il monopattino? Principalmente chiederei i dati presenti sui documenti di identità
    2. Pensi che la registrazione di foto dell'utente sia necessaria per scopi di sicurezza o verifica? è meglio una foto di un documento di identità
    3. Come affronteresti la gestione dei dati personali in conformità alle normative sulla privacy? Voglio rispettare le normative
 
3. Analizzare il comportamento del conducente: 
    1. Pensi di voler analizzare il comportamento dei conducenti durante il noleggio? In caso affermativo, quali metriche o comportamenti specifici desideri monitorare? è una buona idea. In questo modo è possibile limitare l’uso dei monopattini a persone che assumo comportamenti sbagliati, come andare oltre ai limiti di velocità o abbandonare il monopattino in posti non sicuri
    2. Come useresti i dati sul comportamento dei conducenti per migliorare la sicurezza e l'efficienza del servizio? li userei per vietare l’uso dei monopattini a chi assume comportamenti sbagliati
    3. Hai intenzione di fornire feedback agli utenti basati sul loro comportamento durante il noleggio? No
 
4. Segnalare automaticamente gli scooter che necessitano di manutenzione: 
    1. Vorresti che il sistema identificasse automaticamente i monopattini che richiedono manutenzione in base ai dati raccolti durante il noleggio? Se sì, quali indicatori o segnali dovrebbe considerare? Attraverso una notifica al manutentore e una spia accesa sul monopattino
    2. Quali tipi di problemi meccanici o di sicurezza ritieni dovrebbero innescare automaticamente una segnalazione di manutenzione? Tutti i tipi di guasti meccanici (problemi al motore, freni, gomme)
    3. Hai preferenze sulla modalità di notifica quando un monopattino richiede manutenzione? No
 
5. Conclusione: 
    1. C'e' qualche altra feature di cui non abbiamo parlato che vorresti analizzare? No
    2. C'e' qualche osservazione che a posteriori ti senti di dare sull’intervista? No

## Data Analysis

Durante l'intervista con lo stakeholder sono emersi alcuni requisiti e considerazioni chiave per il sistema di gestione dei monopattini in relazione alle esigenze di noleggio. Di seguito, riassumiamo i punti salienti delle risposte fornite.

## Actual Requirements

### Requisiti Funzionali
- Il sistema *deve* registrare i dettagli del noleggio.
- Il sistema *deve* registrare i dati GPS durante ciascun noleggio.
- Il sistema *deve* tenere traccia degli spostamenti passati.
- Il sistema *deve* registrare le informazioni personali dell'utente.
- Il sistema *deve* analizzare il comportamento del conducente.
- Il sistema *deve* vietare il noleggio a chi ha assunto comportamenti sbagliati.
- Il sistema *dovrebbe* segnalare automaticamente gli scooter che necessitano di manutenzione.

### Requisiti Non Funzionali
- Il sistema *deve* richiedere i dati presenti sul documento d'identità dell'utente affinché il noleggio sia possibile.
- Il sistema *potrebbe* richiedere di caricare la foto del documento d'identità dell'utente.
- Il sistema *deve* garantire la sicurezza dei dati dell'utente.
- Il sistema *deve* garantire la sicurezza dei dati di tracciamento GPS (velocità e percorso dello scooter), i quali sono raccolti in ogni momento (tramite sync), con un impatto minimo sulle prestazioni.
- Il sistema *deve* garantire la sicurezza dei dati di tracciamento GPS relativi agli spostamenti passati.
- Il sistema *deve* essere in grado di monitorare contemporaneamente un grande numero di noleggi.
- Il sistema *deve* mantenere un alto livello di disponibilità, garantendo che gli amministratori possano accedere e gestire i dati dei noleggi in qualsiasi momento.
- Il sistema *deve* essere conforme alle leggi sulla privacy dei dati dei conducenti e degli utenti.
- Il sistema *deve* avere la capacità di effettuare controlli di manutenzione e generare rapporti di manutenzione in modo efficiente, senza causare significativi ritardi nelle operazioni regolari del sistema.
- Il sistema *deve* inviare la segnalazioni di manutenzione al manutentore attraverso una notifica.
- Il sistema *deve* inviare un segnale allo scooter per accendere la spia di manutenzione in caso di problemi (guasti meccanici, problemi al motore, freni e gomme).
- Il sistema *deve* raccogliere le informazioni relative al comportamento del conducente, analizzarle e stabilire se la guida si è svolta in maniera corretta, vietando la possibilità di noleggio all'utente in caso di violazione delle norme.
- Il sistema *dovrebbe* anonimizzare l'analisi del comportamento del conducente per renderla accessibile solo al personale autorizzato.
