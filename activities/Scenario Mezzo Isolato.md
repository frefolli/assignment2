# Scenario Mezzo Isolato 

## Requirements Class

Requisiti in merito al dover gestire scenari di mezzi che non comunicano più con il sistema.

## Description

Questa attività è uno Scenario Negativo. 
In questo Scenario ipotizziamo che un mezzo smetta di comunicare con il sistema, questo verifica che la condizione perduri per un certo lasso di tempo, e in caso il sistema notifichi di far intervenire un addetto a controllare il mezzo.


## Stakeholders

Alessandro Gilardi

## Reason

Contattiamo il cliente per avere riscontro sulla nostra effettiva comprensione di questo requisito, in quanto lo stakeholder stesso ci ha già comunicato quanto gli prema risolvere efficacemente l'inconveniente dei mezzi isolati.

## Structure 

### Contesto: 

Nota: va definita a priori un lasso di tempo "T1" che, se superato, permetta di ritenere i mezzi irresponsivi effettivamente isolati.
I mezzi comunicano periodicamente con il sistema la loro posizione, insieme ad altri dati.
Assumiamo inoltre che sia stato definito questo intervallo temporale (ogni quanto comunicano).

### scenario

Si consideri un mezzo della flotta, attualmente noleggiato da un cliente finale oppure no.
Questo mezzo "salta" una comunicazione periodica al sistema.
Il sistema si accorge della mancato arrivo dei dati del mezzo e inizia a fare un conto alla rovescia fino a T1 minuti.
Se il conto alla rovescia scade, il sistema riterrà il mezzo isolato.
Se arriva una comunicazione da questo mezzo prima che venga ritenuto isolato, il conto alla rovescia viene interrotto e lo stato del mezzo ritorna normale.
Quando un mezzo diventa *isolato* il sistema comunica l'evento emettendo una notifica ai gestori del sistema.
La notifica contiene l'ultima posizione nota del mezzo. I gestori disporranno loro l'inivio di un addetto che vada a controllare il mezzo.

## Additional notes

Il parametro della soglia di tempo T1 è stato introdotto per evitare che una condizione di irreperibilità transitoria che poi si risolve spontaneamente (come il transito in una galleria), generi una richiesta di intervento.

## Raw Data Collection

Alessandro Gilardi: lo scenario risponde esattamente a quanto desidero che faccia il vostro sistema. È importante che le notifiche siano chiare e leggibili da tutti.

## Data Analysis

Lo scenario produce i requisiti che scriviamo nella sezione a seguire.

## Actual Requirements

### Requisiti funzionali

il sistema *deve* accorgersi quando un mezzo dovrebbe sincronizzarsi ma non lo fa.
il sistema *deve* tollerare per un certo periodo la disconnessione di un mezzo.
il sistema *deve*, superato il periodo di tolleranza per la disconnessione di un mezzo, inviare una notifica ai gestori sul mezzo isolato.

### Non Funzionali

Le notifiche al gesore *devono* essere facili da interpretare.
