# Dominio della manutenzione

## Requirements

Questa attivita' studia i requisiti relativi ai dati di manutenzione che potrebbero dover essere tracciati.

## Description

Un background study su altri sistemi di gestione della manutenzione e sulle entità coinvolte. E' stato prodotto come artefatto un'istanza del modello di dominio per tale sottosistema.

## Stakeholders

Nessun stakeholder coinvolto.

## Reason

Può essere utile entrare nel dominio dell'applicazione per capire quali potrebbero essere le esigenze di un cliente relative alle manutenzioni ed i dati ad esse collegati.
