# Requisiti di Manutenzione
## Requirements Class

Questa attività estrae i requisiti per i dati di manutenzione che dovrebbero essere tracciati, con la radicalita' di tale tracciamento e dei servizi connessi di cui dovremmo occuparci.

## Description

Un'intervista ad alcuni stakeholder sulle loro esigenze legate alla manutenzione e quanto dovrebbe essere approfondita l'anagrafica del dominio. Sono stati prodotti come artefatto i dati grezzi dell'intervista, un modello di dominio aggiornato per tale sottosistema e l'elenco dei requisiti emersi.

## Stakeholders

Alessandro Gilardi.

## Reason

Chiediamo direttamente ai clienti quali sono le loro reali esigenze per affinare la nostra visione sul sistema che sarà.

## Structure

1. Manutenzioni:
    1. Hai in questo momento più di un manutentore o pianifichi di averlo in futuro?
    2. Sei interessato a monitorare dove viene manutenuto un veicolo?
    3. Che tipo di dati delle manutenzioni vorresti tenere traccia?
    4. Attualmente hai la necessità di gestire in modo diverso qualche tipo di manutenzione? (preventive, correttive, revisioni...)
    5. Pensi di poter avere più interventi nell'ambito di una manutenzione? (intervento manutentivo)
2. Piani di manutenzione:
    1. Poiche' la revisione periodica dei veicoli e' importante per garantirne la massima efficienza, tenere traccia dei piani di revisione (rafforzati dalla legge o personalizzati) potrebbe essere una funzionalità utile?
    2. E' sufficiente tenere traccia solo del piano di manutenzione attuale di un veicolo?
    3. I piani di manutenzione dovrebbero essere organizzabili in gruppi per una migliore gestione degli stessi?
    4. E' ragionevole pensare di dividere i piani di manutenzione in regole che definiscano limiti per le scadenze, sei d'accordo?
    5. Potrebbe essere utile avere le regole raggruppate per categorie? (questo può facilitare la distinzione tra diversi tipi di regole)
    6. Quali unità di valore dovrebbe essere in grado di governare una regola? Qualcosa come (km e ore di servizio, date) dovrebbe essere sufficiente?
3. Scadenze:
    1. Il calcolo automatico delle scadenze di revisione potrebbe essere una funzionalita' utile?
    2. Stai considerando di prevedere tolleranze per le scadenze?
    3. Dato che le scadenze verrebbero calcolate a partire dall'ultima manutenzione, vorresti che le scadenze venissero calcolate anche per i veicoli nuovi senza revisioni precedenti?
    4. Se si', e' preferibile poter inserire (manualmente o tramite sync) questi dati o meglio avere solo valori predefiniti? (0 per i valori numerici, data di inserimento per le date)
4. Pezzi di ricambio:
    1. Sei interessato a conservare i dati sulla composizione (= pezzi di ricambio) dei veicoli?
    2. Che tipo di dati dei pezzi di ricambio vorresti tenere traccia?
    3. I vostri ricambi sono soggetti a revisione?
    4. Vuoi tenere traccia delle scadenze anche per i pezzi di ricambio?
    5. Un veicolo verrebbe considerato "scaduto" se alcune scadenze sono state raggiunte (fuori dalla tolleranza se prevista), le scadenze dei pezzi di ricambio dovrebbero contribuire a questo stato?
    6. Un veicolo deve essere considerato fuori servizio se scaduto?
5. Conclusione:
    1. c'e' qualche altra feature di cui non abbiamo parlato che vorresti analizzare?
    2. c'e' qualche osservazione che a posteriori ti senti di dare sull'intervista?

## Raw Data Collection

Si riportano per intero le risposte dell'intervistato.

1. Manutenzioni:
    1. Hai in questo momento più di un manutentore o pianifichi di averlo in futuro? Per ora solo uno, ma in futuro pianifico di averne più di uno
    2. Sei interessato a monitorare dove viene manutenuto un veicolo? Sì
    3. Che tipo di dati delle manutenzioni vorresti tenere traccia? Data e tipo di manutenzione, chilometri percorsi, parti sostituite e costi associati.
    4. Attualmente hai la necessità di gestire in modo diverso qualche tipo di manutenzione? (preventive, correttive, revisioni...) No
    5. Pensi di poter avere più interventi nell'ambito di una manutenzione? (intervento manutentivo) Sì
2. Piani di manutenzione:
    1. Poiche' la revisione periodica dei veicoli e' importante per garantirne la massima efficienza, tenere traccia dei piani di revisione (rafforzati dalla legge o personalizzati) potrebbe essere una funzionalità utile? Certamente sì
    2. E' sufficiente tenere traccia solo del piano di manutenzione attuale di un veicolo? No, anche di quelli passati
    3. I piani di manutenzione dovrebbero essere organizzabili in gruppi per una migliore gestione degli stessi? Sì
    4. E' ragionevole pensare di dividere i piani di manutenzione in regole che definiscano limiti per le scadenze, sei d'accordo? Sì sono daccordo
    5. Potrebbe essere utile avere le regole raggruppate per categorie? (questo può facilitare la distinzione tra diversi tipi di regole) Sì sarebbe utile
    6. Quali unità di valore dovrebbe essere in grado di governare una regola? Qualcosa come (km e ore di servizio, date) dovrebbe essere sufficiente? Sì, le regole dovrebbero essere governate da unità di valore come chilometri percorsi, ore di servizio e date.
3. Scadenze:
    1. Il calcolo automatico delle scadenze di revisione potrebbe essere una funzionalita' utile? Sì
    2. Stai considerando di prevedere tolleranze per le scadenze? No, devono essere tassative
    3. Dato che le scadenze verrebbero calcolate a partire dall'ultima manutenzione, vorresti che le scadenze venissero calcolate anche per i veicoli nuovi senza revisioni precedenti? Sì per utilità
    4. Se si', e' preferibile poter inserire (manualmente o tramite sync) questi dati o meglio avere solo valori predefiniti? (0 per i valori numerici, data di inserimento per le date) Tramite sync
4. Pezzi di ricambio:
    1. Sei interessato a conservare i dati sulla composizione (= pezzi di ricambio) dei veicoli? Sì
    2. Che tipo di dati dei pezzi di ricambio vorresti tenere traccia? Nome del pezzo, numero di serie, fornitore e costo.
    3. I vostri ricambi sono soggetti a revisione? Sì
    4. Vuoi tenere traccia delle scadenze anche per i pezzi di ricambio? Sì
    5. Un veicolo verrebbe considerato "scaduto" se alcune scadenze sono state raggiunte (fuori dalla tolleranza se prevista), le scadenze dei pezzi di ricambio dovrebbero contribuire a questo stato? Sì
    6. Un veicolo deve essere considerato fuori servizio se scaduto? Certamente
5. Conclusione:
    1. c'e' qualche altra feature di cui non abbiamo parlato che vorresti analizzare? No
    2. c'e' qualche osservazione che a posteriori ti senti di dare sull'intervista? Nessuna

## Data Analysis

Il customer e' interessato a gestire in modo semplice le manutenzioni, non rinunciando pero' a tenere traccia di piu' interventi e di tutte le informazioni relative alle manutenzioni, compresi i manutentori che possono essere molteplici.

Il customer e' interessato a gestire lo storico dei piani di manutenzione associati ad un veicolo, opportunamente organizzati per livelli e gruppi in modo da semplificarne la gestione.

Il customer e' interessato al calcolo automatico delle scadenze, senza tolleranze e con dati aggiornabili solo tramite sync, e default per i veicoli nuovi senza manutenzioni precedenti.

## Actual Requirements

### Funzionali

Manutenzioni:
- Il sistema *deve* permettere all'utente di gestire le informazioni sui manutentori.
- Il sistema *deve* permettere all'utente di gestire le informazioni sulle officine.
- Il sistema *deve* permettere all'utente di gestire le informazioni sulle manutenzioni.
- Il sistema *deve* permettere all'utente di gestire le informazioni sugli interventi.
- Il sistema *deve* permettere all'utente di gestire l'associazione tra il veicolo e il soggetto manutentore.

Piani di Manutenzione:
- Il sistema *deve* permettere all'utente di gestire le informazioni sui piani di manutenzione.
- Il sistema *deve* permettere all'utente di gestire l'associazione tra il veicolo e il piano di manutenzione.
- Il sistema *deve* permettere all'utente di vedere lo storico dei piani di manutenzione per un veicolo.
- Il sistema *deve* permettere all'utente di gestire le informazioni sulle regole dei piani di manutenzione.
- Il sistema *deve* permettere all'utente di organizzare le regole in categorie.
- Il sistema *deve* permettere all'utente di organizzare i piani in gruppi.

Scadenze:
- Il sistema *deve* calcolare in automatico le scadenze associate ad un piano di manutenzione in base all'ultima manutenzione effettuata.
- Il sistema *deve* considerare l'inserimento di un veicolo come "ultima manutenzione" per i veicoli senza manutenzioni precedenti.
- Il sistema *deve* permettere ad un agente esterno autorizzato di aggiornare i punti zero tramite sync.
- Il sistema *deve* considerare "fuori servizio" un veicolo se alcune scadenze sono state raggiunte
- Il Sistema *deve* mantenere le informazioni sulle medie giornaliere (degli ultimi 30 giorni) di uso di un veicolo per il calcolo delle scadenze.

Pezzi di ricambio:
- Il sistema *deve* permettere all'utente di gestire le informazioni sui pezzi di ricambio.
- Il sistema *deve* permettere all'utente di gestire la composizione di un veicolo.
- Il sistema *deve* permettere all'utente di gestire l'associazione tra il pezzo di ricambio e il piano di manutenzione.

### Non Funzionali

- Il Sistema *dovrebbe* memorizzare le seguenti informazioni sulle medie: (veicolo, unita', valore, indice)
- Il Sistema *dovrebbe* memorizzare le seguenti informazioni per il manutentore: (nome, descrizione)
- Il Sistema *dovrebbe* memorizzare le seguenti informazioni per l'officina: (nome, luogo, descrizione, manutentore)
- Il Sistema *dovrebbe* memorizzare le seguenti informazioni per la manutenzione: (veicolo, officina, status, tipo, data inizio, data fine)
- Il Sistema *dovrebbe* memorizzare le seguenti informazioni per l'intervento: (manutenzione, data, attivita', costo)
- Il Sistema *dovrebbe* memorizzare le seguenti informazioni per il piano di manutenzione: (nome, descrizione)
- Il Sistema *dovrebbe* memorizzare le seguenti informazioni per la categoria: (nome)
- Il Sistema *dovrebbe* memorizzare le seguenti informazioni per la regola: (nome, criteri: km, ore di servizio e date)
- Il Sistema *dovrebbe* memorizzare le seguenti informazioni per i gruppi di PDM: (nome, descrizione)
- Il Sistema *dovrebbe* memorizzare le seguenti informazioni per il pezzo di ricambio: (nome, numero di serie, fornitore, veicolo, data di installazione, costo)
- Il Sistema *dovrebbe* memorizzare le seguenti informazioni per la scadenza: (veicolo, [pezzo], PDM, categoria, regola, valori, data)
- Il Sistema *dovrebbe* memorizzare le seguenti informazioni per i punti zero: (veicolo, [pezzo], PDM, categoria, regola, valori, data)
- Il Sistema *dovrebbe* effettuare il calcolo delle scadenze solo quando viene modificato qualche dato o viene introdotto un piano di manutenzione per un veicolo
- Il Sistema *dovrebbe* effettuare il calcolo della previsione della data per i criteri non su date, ogni notti e quando viene calcolata la scadenza.
