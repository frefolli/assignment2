# Scenario scadenza manutenzione

## Requirements Class

Noi ipotizziamo che il sistema abbia il requisito di dover informare i gestori della piattaforma in merito alle scadenze delle manutenzioni. 

## Description

Questa attività è uno Scenario Anormale. 
Lo scenario descritto ipotizza la scadenza  della manutenzione  di un veicolo della flotta.


## Stakeholders

Alessandro Gilardi

## Reason

Consideriamo questo scenario perchè lo stakeholder ci ha già detto che per lui sarebbe un costo eccessivo gestire manualmete le scadenze di manutenzione e vorrebbe che il sistema avesse questa funzionalità. 
Contattiamo il cliente per avere riscontro sulla nostra effettiva comprensione di questo requisito del sistema, sottoponendogli questo scenario.

## Structure 

### Contesto: 

Le parti di ogni veicolo della flotta si usurano, e quindi richiedono della costante manutenzione ordinaria. L'operazione di manutenzione delle parti diventa obbligatoria allo scadere dell'ultima  manutenzione della parte stessa. A seconda del tipo di pezzo, una manutenzione scade in base ai chilometri percorsi o al tempo passato dall'ultima manutenzione.
I veicoli comunicano costantemente al sistema dei dati, tra cui la distanza percorsa dal mezzo dalla messa in esercizio.
Nota: Lo scenario inizia considerando un veicolo non attualmete in uso da un cliente.

### Scenario

Accade che una parte di un veicolo raggiunge il limite di chilometri percorribili dall'ultima manutenzione.
Il mezzo, come fa periodicamente, comunica il dato aggiornato della distanza totale percorsa.
Il sistema elabora il dato e comprende che la mutenzione della parte è scaduta.
Lo scenario prosegue con il sistema che formula una notifica dell'evento e la inoltra ad un addetto preposto. 
Contestualmente il sistema la modifica dello stato del veicolo in scaduto.
Il mezzo ora non potrà più essere preso a noleggio da un cliente.


## Additional notes

Nessuna.

## Raw Data Collection

Alessandro Gilardi: Bene lo scenario. Faccio notare di come per me sia importante che il sistema di notifiche sia semplice e di immediata comprensione per gli addetti.

## Data Analysis

Lo scenario produce i requisiti che scriviamo nella sezione a seguire.

## Actual Requirements

### Funzionali

il sistema *deve* contemplare l' invio costante della distanza percorsa dai  mezzi elettrici

Il sistema *deve* impedire il noleggio di un veicolo con manutenzione scaduta.

Il sistema *deve* notificare i gestori dello scadere della manutenzione di un veicolo.

### Non Funzionali

Le notifiche al gesore *devono* essere facili da interpretare.
