# Dominio del noleggio

## Requirements

Questa attivita' studia i requisiti relativi ai dati di noleggio che saranno tracciati.

## Description

Background study: uno studio di base su altri sistemi di gestione dell'attività di noleggio.

## Stakeholders

Nessun stakeholder coinvolto

## Reason

Può essere utile entrare nel dominio dell'applicazione per capire quali potrebbero essere le esigenze di un cliente relative all'acquisizione di informazioni fondamentali per il noleggio.
