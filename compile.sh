#!/bin/bash

ENGINE_D2=elk #in [elk, dagre]
INPUTS_D2=$(find . -type f | grep .*\.yml$)
INPUTS_PUML=$(find . -type f | grep .*\.puml$)
INPUTS_TEX=$(find . -type f | grep .*\.tex$)

function compile_with_d2() {
    input=$1
    dst=${input/yml/png}
    if [ $input -nt $dst ]; then
        d2 $input $dst -l $ENGINE_D2
    fi
}

function compile_with_puml() {
    input=$1
    dst=${input/puml/png}
    if [ $input -nt $dst ]; then
        plantuml $input
    fi
}

function compile_with_tex() {
    input=$1
    pdflatex $input && pdflatex $input
}

function main() {
    for input in $INPUTS_D2; do
        compile_with_d2 $input
    done

    for input in $INPUTS_PUML; do
        compile_with_puml $input
    done

    for input in $INPUTS_TEX; do
        compile_with_tex $input
    done
}

main
