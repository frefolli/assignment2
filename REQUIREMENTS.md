# Requirements

## User abilities

## Requisiti di Manutenzione

### Funzionali

Manutenzioni:
- Il sistema *deve* permettere all'utente di gestire le informazioni sui manutentori.
- Il sistema *deve* permettere all'utente di gestire le informazioni sulle officine.
- Il sistema *deve* permettere all'utente di gestire le informazioni sulle manutenzioni.
- Il sistema *deve* permettere all'utente di gestire le informazioni sugli interventi.
- Il sistema *deve* permettere all'utente di gestire l'associazione tra il veicolo e il soggetto manutentore.

Piani di Manutenzione:
- Il sistema *deve* permettere all'utente di gestire le informazioni sui piani di manutenzione.
- Il sistema *deve* permettere all'utente di gestire l'associazione tra il veicolo e il piano di manutenzione.
- Il sistema *deve* permettere all'utente di vedere lo storico dei piani di manutenzione per un veicolo.
- Il sistema *deve* permettere all'utente di gestire le informazioni sulle regole dei piani di manutenzione.
- Il sistema *deve* permettere all'utente di organizzare le regole in categorie.
- Il sistema *deve* permettere all'utente di organizzare i piani in gruppi.

Scadenze:
- Il sistema *deve* calcolare in automatico le scadenze associate ad un piano di manutenzione in base all'ultima manutenzione effettuata.
- Il sistema *deve* considerare l'inserimento di un veicolo come "ultima manutenzione" per i veicoli senza manutenzioni precedenti.
- Il sistema *deve* permettere ad un agente esterno autorizzato di aggiornare i punti zero tramite sync.
- Il sistema *deve* considerare "fuori servizio" un veicolo se alcune scadenze sono state raggiunte
- Il Sistema *deve* mantenere le informazioni sulle medie giornaliere (degli ultimi 30 giorni) di uso di un veicolo per il calcolo delle scadenze.

Pezzi di ricambio:
- Il sistema *deve* permettere all'utente di gestire le informazioni sui pezzi di ricambio.
- Il sistema *deve* permettere all'utente di gestire la composizione di un veicolo.
- Il sistema *deve* permettere all'utente di gestire l'associazione tra il pezzo di ricambio e il piano di manutenzione.

### Non Funzionali

- Il Sistema *dovrebbe* memorizzare le seguenti informazioni sulle medie: (veicolo, unita', valore, indice)
- Il Sistema *dovrebbe* memorizzare le seguenti informazioni per il manutentore: (nome, descrizione)
- Il Sistema *dovrebbe* memorizzare le seguenti informazioni per l'officina: (nome, luogo, descrizione, manutentore)
- Il Sistema *dovrebbe* memorizzare le seguenti informazioni per la manutenzione: (veicolo, officina, status, tipo, data inizio, data fine)
- Il Sistema *dovrebbe* memorizzare le seguenti informazioni per l'intervento: (manutenzione, data, attivita', costo)
- Il Sistema *dovrebbe* memorizzare le seguenti informazioni per il piano di manutenzione: (nome, descrizione)
- Il Sistema *dovrebbe* memorizzare le seguenti informazioni per la categoria: (nome)
- Il Sistema *dovrebbe* memorizzare le seguenti informazioni per la regola: (nome, criteri: km, ore di servizio e date)
- Il Sistema *dovrebbe* memorizzare le seguenti informazioni per i gruppi di PDM: (nome, descrizione)
- Il Sistema *dovrebbe* memorizzare le seguenti informazioni per il pezzo di ricambio: (nome, numero di serie, fornitore, veicolo, data di installazione, costo)
- Il Sistema *dovrebbe* memorizzare le seguenti informazioni per la scadenza: (veicolo, [pezzo], PDM, categoria, regola, valori, data)
- Il Sistema *dovrebbe* memorizzare le seguenti informazioni per i punti zero: (veicolo, [pezzo], PDM, categoria, regola, valori, data)
- Il Sistema *dovrebbe* effettuare il calcolo delle scadenze solo quando viene modificato qualche dato o viene introdotto un piano di manutenzione per un veicolo
- Il Sistema *dovrebbe* effettuare il calcolo della previsione della data per i criteri non su date, ogni notti e quando viene calcolata la scadenza.

## Maintenance integration (Maintenance Works e/o Maintenances: preventive, corrective ...)
Il sistema *deve* impedire il noleggio di un veicolo con manutenzione scaduta.

Il sistema *deve* notificare i gestori dello scadere della manutenzione di un veicolo.
## Multi-Tenancy

## Administration data (info of entities)

## Sync needs and integration

## Rental info and depth

### Requisiti Funzionali
- Il sistema *deve* registrare i dettagli del noleggio.
- Il sistema *deve* registrare i dati GPS durante ciascun noleggio.
- Il sistema *deve* tenere traccia degli spostamenti passati.
- Il sistema *deve* registrare le informazioni personali dell'utente.
- Il sistema *deve* analizzare il comportamento del conducente.
- Il sistema *deve* vietare il noleggio a chi ha assunto comportamenti sbagliati.
- Il sistema *dovrebbe* segnalare automaticamente gli scooter che necessitano di manutenzione.

### Requisiti Non Funzionali
- Il sistema *deve* richiedere i dati presenti sul documento d'identità dell'utente affinché il noleggio sia possibile.
- Il sistema *potrebbe* richiedere di caricare la foto del documento d'identità dell'utente.
- Il sistema *deve* garantire la sicurezza dei dati dell'utente.
- Il sistema *deve* garantire la sicurezza dei dati di tracciamento GPS (velocità e percorso dello scooter), i quali sono raccolti in ogni momento (tramite sync), con un impatto minimo sulle prestazioni.
- Il sistema *deve* garantire la sicurezza dei dati di tracciamento GPS relativi agli spostamenti passati.
- Il sistema *deve* essere in grado di monitorare contemporaneamente un grande numero di noleggi.
- Il sistema *deve* mantenere un alto livello di disponibilità, garantendo che gli amministratori possano accedere e gestire i dati dei noleggi in qualsiasi momento.
- Il sistema *deve* essere conforme alle leggi sulla privacy dei dati dei conducenti e degli utenti.
- Il sistema *deve* avere la capacità di effettuare controlli di manutenzione e generare rapporti di manutenzione in modo efficiente, senza causare significativi ritardi nelle operazioni regolari del sistema.
- Il sistema *deve* inviare la segnalazioni di manutenzione al manutentore attraverso una notifica.
- Il sistema *deve* inviare un segnale allo scooter per accendere la spia di manutenzione in caso di problemi (guasti meccanici, problemi al motore, freni e gomme).
- Il sistema *deve* raccogliere le informazioni relative al comportamento del conducente, analizzarle e stabilire se la guida si è svolta in maniera corretta, vietando la possibilità di noleggio all'utente in caso di violazione delle norme.
- Il sistema *dovrebbe* anonimizzare l'analisi del comportamento del conducente per renderla accessibile solo al personale autorizzato.
