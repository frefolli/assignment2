# Workflow

## Project

Vehicle Rental & Management System

## Stakeholders

Alessandro Gilardi, studente della Laurea Magistrale di Informatica, e' interessato a sviluppare un sistema che gli permetta di gestire la flotta di veicoli e il noleggio degli stessi per gli utenti del capoluogo meneghino.

## Workflow

![Workflow](workflow.png)

# Activities

Elenco delle attivita':
- [Dominio delle manutenzioni](activities/maintenance_world.md)
- [Requisiti di manutenzione](activities/maintenance_needs.md)
- [Dominio dei noleggi](activities/rental_world.md)
- [Requisiti di noleggio](activities/rental_needs.md)
- [Scenario di Guasto](activities/rental_scenario_guasto.md)
- [Scenario Mezzo Isolato](activities/Scenario\ Mezzo\ Isolato.md)
- [Scenario scadenza della revisione](activities/Scenario\ scadenza\ manutenzione.md)
- [Scenario batteria scarica](activities/scenario\ batteria\ scarica.md)
- [Nuovo scenario batteria scarica](activities/Nuovo\ Scenario\ Batteria\ Scarica.md)

